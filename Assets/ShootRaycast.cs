﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Parabox.CSG;

public class ShootRaycast : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ShootRay();
        }
    }

    void ShootRay()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;

            try
            {              
                var impact = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                impact.transform.position = hit.point;
                impact.transform.localScale = Vector3.one * 0.5f;

                var createDebris = CSG.Intersect(hit.transform.gameObject, impact);
                var createSubtract = CSG.Subtract(hit.transform.gameObject, impact);

                // Debris Composite
                var dComposite = new GameObject();
                dComposite.AddComponent<MeshFilter>().sharedMesh = createDebris.mesh;
                dComposite.AddComponent<MeshRenderer>().sharedMaterials = createDebris.materials.ToArray();
                dComposite.AddComponent<MeshCollider>();
                dComposite.AddComponent<DestroyableObject>();
                dComposite.transform.name = "Debris";

                // Substract Composite
                var sComposite = new GameObject();
                sComposite.AddComponent<MeshFilter>().sharedMesh = createSubtract.mesh;
                sComposite.AddComponent<MeshRenderer>().sharedMaterials = createSubtract.materials.ToArray();
                sComposite.AddComponent<MeshCollider>();
                sComposite.AddComponent<DestroyableObject>();

                Destroy(hit.transform.gameObject);
                Destroy(impact);

                var destroyLogic = dComposite.GetComponent<DestroyableObject>();
                destroyLogic.ExplodeForce = 5;
                destroyLogic.CutCascades = 2;
                destroyLogic.DestroyMesh(-ray.direction, sComposite.transform.InverseTransformPoint(hit.point));   
            }
            catch
            {
                return;
            }
        }
    }
}
